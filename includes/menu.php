<?php  
function activar($penlace){
	global $p;
	if($penlace==$p){
		return 'active';
	}else{
		return '';
	}
}

?>
<!-- <ul class="nav nav-tabs">
  <li class="<?php if($p=='inicio.php'){echo 'active';};?>"><a href="index.php?p=inicio.php">Inicio</a></li>
  <li class="<?php if($p=='listado.php'){echo 'active';};?>"><a href="index.php?p=listado.php">Listado de productos</a></li>
  <li class="<?php if($p=='insertar.php'){echo 'active';};?>"><a href="index.php?p=insertar.php">Alta de producto</a></li>
  <li class="<?php if($p=='listadoPorCategoria.php'){echo 'active';};?>"><a href="index.php?p=listadoPorCategoria.php">Por Categorias</a></li>
</ul> -->

<!-- <ul class="nav nav-tabs">
  <li class="<?php echo activar('inicio.php');?>"><a href="index.php?p=inicio.php">Inicio</a></li>
  <li class="<?php echo activar('listado.php');?>"><a href="index.php?p=listado.php">Listado de productos</a></li>
  <li class="<?php echo activar('insertar.php');?>?>"><a href="index.php?p=insertar.php">Alta de producto</a></li>
  <li class="<?php echo activar('listadoPorCategoria.php');?>?>"><a href="index.php?p=listadoPorCategoria.php">Por Categorias</a></li>
</ul> -->

<!-- asi queda marcado el enlace seleccionado -->
<?php  
$urlNombre=array('Inicio','Listado de productos','Alta de producto','Por Categorias', 'Contactanos', 'Gestion de Enlaces');
$urlEnlace=array('inicio.php','listado.php','insertar.php','listadoPorCategoria.php&idCat=1', 'contacto.php', 'listadoEnlaces.php');
?>
<ul class="nav nav-tabs">
	<?php 
	for($i=0;$i<count($urlNombre);$i++){
		if($urlEnlace[$i]==$p){
			$c='active';
		}else{
			$c='';
		}
	?>
		<li class="<?php echo $c;?>">
			<a href="index.php?p=<?php echo $urlEnlace[$i];?>">
				<?php echo $urlNombre[$i];?>
			</a>
		</li>
	<?php  
	}
	?>
</ul>